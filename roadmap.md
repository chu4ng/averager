# Averager
Blahblah TODO

TODO:
- [ ] consume FIO from kafka
    - [x] read something about kafque
    - [ ] can I create a "topic" in code?
    - [x] add kafka script for produser
    - [x] write kafka consumer
    - [ ] if encountering error place order to FIO_FAILED kafka-queue
- [x] make request to api.agify.io?name=Name etc , add new fields
- [ ] send struct to postgres
    - [x] mocks
    - [ ] tested and works
- [ ] handle rest requests
    - [x] Add query
- [ ] make graphql
- [ ] redis caching
    - [x] mocks
    - [ ] tested works
- [ ] satisfy linter
    - [ ] wrapcheck errorlint gomnd // logic
    - [ ] nlreturn wsl whitespace // spaces style
    - [ ] golint gocritic stylecheck // code style
    - [ ] misspell
add:
- [ ] logs
- [ ] tests
- [ ] configuration
- [ ] move redis, kafka, postgress to docker

messages from kafka:
```json
{
    "name": "Someone",
    "surname": "I-don'-know",
    "patronymic": "Really now?" // not required
}
```
