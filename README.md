# Averager
## Description
TODO
This service listen for kafka query and using third-patry API expands given message with assumed data(age, gender etc).
RESTApi works as CRUD and not yet implemented.

Server uses:
- Redis as cache
- Postgres as database
- GraphQL
- Kafka as message broker
- external services https://api.agify.io https://api.genderize.io https://api.nationalize.io
- gofiber/fiber as web-framework
