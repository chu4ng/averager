build:
	go build -o ./bin ./cmd/server
run: build
	bin/server

produce:
	kafka-console-producer.sh --topic FIO --bootstrap-server localhost:9092 <FIO_payload

clean:
	rm bin/*

read:
	kafka-console-consumer.sh --topic FIO --from-beginning --bootstrap-server localhost:9092
payload:
	curl --json '{"Name":"Alex", "Surname":"Noname", "Patronymic":"None"}' http://127.0.0.1:3000/FIO
rctopic:
	kafka-topics.sh --bootstrap-server :9092 --topic FIO --delete
	kafka-topics.sh --bootstrap-server :9092 --topic FIO --create

