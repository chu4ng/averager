package types

type FIO struct {
	UUID        string `json:"uuid" pg:"uuid"`
	Name        string `json:"name" pg:"name"`
	Surname     string `json:"surname" pg:"surname"`
	Patronymic  string `json:"patronymic" pg:"patronymic"`
	Age         int    `json:"age" pg:"age"`
	Gender      string `json:"gender" pg:"gender"`
	Nationality string `json:"nationality" pg:"nationality"`
}
