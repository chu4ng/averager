package logic

import (
	"log"

	"github.com/google/uuid"
	"gitlab.com/chu4ng/humaverager/pkg/coder"
	"gitlab.com/chu4ng/humaverager/pkg/kafka"
	"gitlab.com/chu4ng/humaverager/pkg/storer"
	"gitlab.com/chu4ng/humaverager/types"
)

type Averager struct {
	Kafka *consumer.Consumer
	// TODO
	Cache storer.Storer
}

func NewAveragerService(kafkas *consumer.Consumer, storer storer.Storer) *Averager {
	return &Averager{
		Kafka: kafkas,
		Cache: storer,
	}
}

// ServeBrokerChannel reads messages from kafka and send all valid events to channel
// If encounter error, sends corrupted messages to FIO_FAILED
func (a *Averager) ServeBrokerChannel() {
	mCh, err := a.Kafka.Consume("FIO")
	if err != nil {
		log.Fatal(err)
	}

	for {
		m := <-mCh
		mStr := m.Value

		fio, err := coder.Unmarshall(mStr)
		if err != nil {
			// TODO Place in FIO_ERROR if encounter error
			log.Printf("ServeFIO: error while unmarshalling, %e\n", err)
			continue
		}

		if invalid := ValidateMessage(fio); invalid {
			// TODO Place in FIO_ERROR if encounter error
			log.Printf("ServeFIO: error while validating")
			continue
		}

		if err := a.handleMessage(fio); err != nil {
			log.Println(err)
			continue
		}
	}

}

// ValidateMessage return true if encounter error
// Error means missing required field
func ValidateMessage(s *types.FIO) bool {
	// TODO Check if fields are strings without special characters
	return s.Name == "" || s.Surname == ""
}

// handleMessage expand FIO-struct with new probability data, then put it into Storer
func (a *Averager) handleMessage(m *types.FIO) error {
	if err := AddInfoAssumtion(m); err != nil {
		return err
	}

	m.UUID = uuid.NewString()

	log.Printf("Storing: %+v\n", *m)

	mm := *m

	if err := a.Cache.Store(mm); err != nil {
		return err
	}

	return nil
}
