package logic

import (
	"encoding/json"
	"io"
	"net/http"
	"strings"

	"gitlab.com/chu4ng/humaverager/types"
)

type Demografix struct {
	Count int    `json:"count"`
	Name  string `json:"name"`
}

type AgeStruct struct {
	Demografix
	Age int `json:"age"`
}

type GenderStruct struct {
	Demografix
	Gender string `json:"gender"`
}

type NationalityStruct struct {
	Country []CountryStruct `json:"country"`
}

type CountryStruct struct {
	Country_id  string  `json:"country_id"`
	Probability float32 `json:"probability"`
}

func AddInfoAssumtion(m *types.FIO) error {

	// TODO make it somehow to for-range
	ageScheme := new(AgeStruct)
	if err := getProbabilityData("https://api.agify.io/?name=", m.Name, ageScheme); err != nil {
		return err
	}
	m.Age = ageScheme.Age

	genderScheme := new(GenderStruct)
	if err := getProbabilityData("https://api.genderize.io/?name=", m.Name, genderScheme); err != nil {
		return err
	}
	m.Gender = genderScheme.Gender

	nationScheme := new(NationalityStruct)
	if err := getProbabilityData("https://api.nationalize.io/?name=", m.Name, nationScheme); err != nil {
		return err
	}
	nationScheme.Country = append(nationScheme.Country, CountryStruct{"NONE", 0})
	m.Nationality = nationScheme.Country[0].Country_id

	return nil
}

func getProbabilityData(link string, name string, scheme interface{}) error {
	resp, err := makeRequest(link, name)
	if err != nil {
		return err
	}

	dec := json.NewDecoder(resp)
	if err = dec.Decode(scheme); err != nil {
		return err
	}

	return nil
}

func SanityzeField(s string) string {
	s = strings.ReplaceAll(s, " ", "%20")
	return s
}

func makeRequest(resource, key string) (io.ReadCloser, error) {
	sanityzedField := SanityzeField(key)
	resp, err := http.Get(resource + sanityzedField)
	if err != nil {
		return nil, err
	}
	return resp.Body, nil
}
