package main

import (
	"log"

	"gitlab.com/chu4ng/humaverager/api"
	"gitlab.com/chu4ng/humaverager/logic"
	"gitlab.com/chu4ng/humaverager/pkg/kafka"
	"gitlab.com/chu4ng/humaverager/pkg/storer"

	fiber "github.com/gofiber/fiber/v2"
)

var _ = consumer.Consumer{}

var (
	RestAddr     = ":3000"
	KafkaBrokers = []string{
		":9092",
	}
	PostgresCreds = storer.StoreCredentials{
		Address:  ":5432",
		UserName: "postgres",
		Password: "",
		Dbname:   "fio",
		Timeout:  5,
	}
	RedisCreds = storer.StoreCredentials{
		Address:  ":6379",
		UserName: "",
		Password: "",
		Dbname:   "1",
		Timeout:  5,
	}
)

func main() {
	var err error
	brokerConsumer := consumer.NewConsumer(KafkaBrokers)
	storerDb, err := storer.NewPostgresStorer(&PostgresCreds)
	if err != nil {
		log.Fatal(err)
	}
	storerCache, err := storer.NewRedisStorer(&RedisCreds, &storerDb)
	if err != nil {
		log.Fatal(err)
	}
	averagerService := logic.NewAveragerService(&brokerConsumer, storerCache)
	handler := api.NewHandler(averagerService)

	app := fiber.New()
	go averagerService.ServeBrokerChannel()

	setupRoutes(app, handler)
	err = app.Listen(RestAddr)
	if err != nil {
		log.Fatal(err)
	}

}

func setupRoutes(app *fiber.App, handler api.Handler) {
	app.Get("/", handler.Root)
	app.Post("/FIO", handler.CreateFIO)
	app.Get("/FIO", handler.RetrieveFIO)
}
