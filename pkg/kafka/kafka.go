package consumer

import (
	"fmt"
	"github.com/Shopify/sarama"
)

type Consumer struct {
	brokers []string
}

func NewConsumer(addrs []string) Consumer {
	return Consumer{
		brokers: addrs,
	}
}

// Consume return channel swith  messages and error
// Currently connects to :9092 and "FIO" topic by default
func (c *Consumer) Consume(topic string) (chan *sarama.ConsumerMessage, error) {
	messages := make(chan *sarama.ConsumerMessage, 256)
	config := sarama.NewConfig()
	consumer, err := sarama.NewConsumer(c.brokers, config)
	if err != nil {
		return nil, err
	}
	partitions, err := consumer.Partitions(topic)
	if err != nil {
		return nil, err
	}
	for _, partition := range partitions {
		pc, err := consumer.ConsumePartition(topic, partition, sarama.OffsetNewest)
		if err != nil {
			return nil, err
		}
		go func(pc sarama.PartitionConsumer) {
			for message := range pc.Messages() {
				messages <- message
			}
		}(pc)
	}

	return messages, err
}

// TODO replace "consumer" with Broker or something
// TODO return errors
func (c *Consumer) Produce(FIOjsonMessage []byte) error {
	config := sarama.NewConfig()

	config.Producer.Return.Successes = true
	config.Producer.Return.Errors = true
	producer, err := sarama.NewSyncProducer(c.brokers, config)
	if err != nil {
		return fmt.Errorf("failed to initialize NewSyncProducer, err: %w", err)
	}
	defer producer.Close()

	msg := &sarama.ProducerMessage{Topic: "FIO", Key: nil, Value: sarama.StringEncoder(FIOjsonMessage)}
	_, _, err = producer.SendMessage(msg)
	if err != nil {
		return fmt.Errorf("failed to send message to kafka: %w", err)
	}

	return nil
}
