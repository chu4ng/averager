package storer

import (
	"context"
	"fmt"
	"strconv"
	"time"

	"github.com/go-redis/redis/v8"
	"gitlab.com/chu4ng/humaverager/pkg/coder"
	"gitlab.com/chu4ng/humaverager/types"
)

var timeout = 10 * time.Second

type redisStorer struct {
	DBStore Storer
	client  *redis.Client
}

// Modify implements Storer.
func (redisStorer) Modify(types.FIO) {
	panic("unimplemented")
}

// Remove implements Storer.
func (redisStorer) Remove(types.FIO) {
	panic("unimplemented")
}

// getCache trying to retrive person by UUID from underlying cache storage,
// if no data exists, it tries to retrieve data from underlying DB storage
// if data exists within DB storage, add into cache and return it
func (r redisStorer) Retrieve(personRequest types.FIO, page int, pageSize int) ([]types.FIO, error) {
	// If UUID specified, use caching and return single entry
	if personRequest.UUID != "" && r.existsIn(personRequest) {
		if p, err := r.getFromCache(personRequest); err != nil || (p == types.FIO{}) {
			return []types.FIO{}, err
		} else {
			return []types.FIO{p}, nil
		}
	}

	// Otherwise simply use psql, I don't want to die trying to juggle with redis-queries
	persons, err := r.DBStore.Retrieve(personRequest, page, pageSize)
	if err != nil {
		return []types.FIO{}, fmt.Errorf("unable to retrive %+v from DBStore: %w", personRequest, err)
	}
	for _, p := range persons {
		if err := r.set(p); err != nil {
			return []types.FIO{}, fmt.Errorf("failed to save entity to cache: %w", err)
		}
	}

	return persons, nil

}

// Store implements Storer.
func (r redisStorer) Store(person types.FIO) error {
	return r.setCache(person, false)
}

func (r redisStorer) getFromCache(personRequest types.FIO) (types.FIO, error) {
	ctx := context.Background()

	pJson, err := r.client.Get(ctx, "person:"+personRequest.UUID).Result()
	if err == redis.Nil {
		return types.FIO{}, fmt.Errorf("person %+v don't exists in redis: %w", personRequest, err)
	}

	person, err := coder.Unmarshall([]byte(pJson))
	if err != nil {
		return types.FIO{}, err
	}

	return *person, nil
}

func (r redisStorer) setCache(personRequest types.FIO, forceStore bool) error {
	if r.existsIn(personRequest) && !forceStore {
		return fmt.Errorf("this %v person already exists in Store, add force flag if you want to rewrite entiry", personRequest)
	}

	if err := r.DBStore.Store(personRequest); err != nil {
		return err
	}

	if err := r.set(personRequest); err != nil {
		return err
	}

	return nil
}

func (r redisStorer) set(personRequest types.FIO) error {
	ctx := context.Background()

	personJson, err := coder.Marshall(personRequest)
	if err != nil {
		return fmt.Errorf("Cache:set: %w", err)
	}
	_, err = r.client.Set(ctx, "person:"+personRequest.UUID, personJson, 0).Result()
	return err
}

func (r redisStorer) existsIn(person types.FIO) bool {
	ctx := context.Background()
	exists := r.client.Exists(ctx, "person:"+person.UUID, "")
	return exists.Val() > 0
}

func NewRedisStorer(cred *StoreCredentials, db *Storer) (Storer, error) {
	redisClient, err := newRedisClient(cred)
	if err != nil {
		return redisStorer{}, err
	}
	return redisStorer{
		DBStore: *db,
		client:  redisClient,
	}, nil
}

func newRedisClient(cred *StoreCredentials) (*redis.Client, error) {
	ctx, cancel := context.WithTimeout(context.Background(), timeout)
	defer cancel()

	redisDbName, err := strconv.Atoi(cred.Dbname)
	if err != nil {
		return &redis.Client{}, fmt.Errorf("can't convert redis db name (%s) to int: %w", cred.Dbname, err)
	}

	rdb := redis.NewClient(&redis.Options{
		Addr:     cred.Address,
		Password: cred.Password,
		DB:       redisDbName,
	})

	_, err = rdb.Ping(ctx).Result()
	return rdb, err
}
