package storer

import (
	"gitlab.com/chu4ng/humaverager/types"
)

type Storer interface {
	Retrieve(p types.FIO, page int, pageSize int) ([]types.FIO, error)
	Store(types.FIO) error
	Remove(types.FIO)
	Modify(types.FIO)
}

type StoreCredentials struct {
	Address  string
	UserName string `yaml:"userName"`
	Password string `yaml:"password"`
	Dbname   string `yaml:"dbname"`
	Timeout  int    `yaml:"timeoutInSeconds"`
}
