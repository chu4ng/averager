package storer

import (
	"context"
	"fmt"
	"time"

	"github.com/go-pg/pg/v10"
	"github.com/go-pg/pg/v10/orm"
	"gitlab.com/chu4ng/humaverager/types"
)

type postgresStorer struct {
	client *pg.DB
}

// Modify implements Storer.
func (postgresStorer) Modify(types.FIO) {
	panic("unimplemented")
}

// Remove implements Storer.
func (postgresStorer) Remove(types.FIO) {
	panic("unimplemented")
}

func (r postgresStorer) Retrieve(p types.FIO, page int, pageSize int) ([]types.FIO, error) {
	var resultPersons []types.FIO

	var query *orm.Query
	query = r.client.Model(&resultPersons)

	if p.UUID != "" {
		query = query.Where("uuid = ?", p.UUID)
	}

	if p.Name != "" {
		query = query.Where("name = ?", p.Name)
	}

	if p.Surname != "" {
		query = query.Where("surname = ?", p.Surname)
	}

	// Pagination
	query = query.Order("uuid ASC").
		Limit(pageSize).
		Offset(page * pageSize)

	if err := query.Select(); err != nil {
		return []types.FIO{}, fmt.Errorf("faialed to execute query: %s", err)
	}

	if len(resultPersons) < 1 {
		return []types.FIO{}, fmt.Errorf("no matched data is saved in postgres: %+v\n", p)
	}
	return resultPersons, nil
}

func (r postgresStorer) Store(request types.FIO) error {
	_, err := r.client.Model(&request).Insert()
	return err
}

func NewPostgresStorer(cred *StoreCredentials) (Storer, error) {
	client, err := newPgClient(cred.UserName, cred.Dbname, cred.Timeout)
	if err != nil {
		return postgresStorer{}, fmt.Errorf("can't connect to postgres server: %w", err)
	}
	return postgresStorer{
		client: client,
	}, err
}

func newPgClient(pgUser, pgName string, pgTimeout int) (*pg.DB, error) {
	ctx, cancel := context.WithTimeout(context.Background(), time.Duration(pgTimeout)*time.Second)
	defer cancel()
	db := pg.Connect(&pg.Options{
		User:     pgUser,
		Database: pgName,
	})
	if err := db.Ping(ctx); err != nil {
		return nil, err
	}
	err := createSchema(db)
	return db, err
}

func createSchema(db *pg.DB) error {
	err := db.Model(&types.FIO{}).CreateTable(&orm.CreateTableOptions{
		Temp:        false,
		IfNotExists: true,
	})
	return err
}
