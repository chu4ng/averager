package coder

import (
	"encoding/json"

	"gitlab.com/chu4ng/humaverager/types"
)

func Unmarshall(s []byte) (*types.FIO, error) {
	fio := &types.FIO{}
	err := json.Unmarshal(s, fio)

	return fio, err
}

func Marshall(m types.FIO) ([]byte, error) {
	b, err := json.Marshal(m)
	if err != nil {
		return []byte{}, err
	}

	return b, nil
}
