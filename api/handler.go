package api

import (
	"fmt"
	"log"

	fiber "github.com/gofiber/fiber/v2"

	"gitlab.com/chu4ng/humaverager/logic"
	"gitlab.com/chu4ng/humaverager/types"
)

type Handler struct {
	service *logic.Averager
}

func NewHandler(a *logic.Averager) Handler {
	return Handler{
		service: a,
	}
}

func (h *Handler) MockDemografix(c *fiber.Ctx) error {
	m := types.FIO{
		Name: "Alex",
	}
	err := logic.AddInfoAssumtion(&m)
	if err != nil {
		return c.SendString(err.Error())
	} else {
		return c.SendString(fmt.Sprintf("%+v\n", m))
	}

}

func (h *Handler) Root(c *fiber.Ctx) error {
	return c.SendString("Hello world")
}

func (h *Handler) CreateFIO(c *fiber.Ctx) error {
	// TODO shall I unmarshall and validate it? fiber has functional for validation, btw
	if err := h.service.Kafka.Produce(c.Body()); err != nil {
		log.Printf("Failed to send msg %s to Kafka: %e\n", c.Body(), err)
		return c.SendStatus(fiber.ErrInternalServerError.Code)
	}
	return c.SendString("create new person" + string(c.Body()))
}

func (h *Handler) RetrieveFIO(c *fiber.Ctx) error {
	page := c.QueryInt("page")
	pageSize := c.QueryInt("pagesize")
	if pageSize == 0 {
		pageSize = 50
	}

	// Refactor to map2struct or something
	uuid := c.Query("uuid")
	name := c.Query("name")
	surname := c.Query("surname")

	person, err := h.service.Cache.Retrieve(types.FIO{UUID: uuid, Name: name, Surname: surname}, page, pageSize)
	if err != nil {
		log.Printf("failed to retrieve from Store, req: %+v, err: %s", c.Queries(), err)
		return fiber.ErrNotFound
	}

	return c.JSON(person)
}
func (h *Handler) UpdateFIO(c *fiber.Ctx) error { return c.SendString("mock") }
func (h *Handler) DeleteFIO(c *fiber.Ctx) error { return c.SendString("mock") }
